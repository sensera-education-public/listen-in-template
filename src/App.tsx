import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './books/Header';
import Main from './books/mainView/Main';

function App() {
  return (
    <div className="App">
    <Header></Header>
    <Main/>
  </div>
  );
}

export default App;
