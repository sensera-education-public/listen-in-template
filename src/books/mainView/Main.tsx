

const books = [
  {
    id: 1,
    title: "Gösta Berlings saga",
    author: "Selma Lagerlöf",
    liked: false,
  },
  {
    id: 2,
    title: "Winter",
    author: "Ali Smith",
    liked: false,
  },
  {
    id: 3,
    title: "The Catcher in the Rye",
    author: "J.D. Salinger",
    liked: false,
  },
]

function Main() {
 
  return (
    <div className="bg">
      <section className="border">
        <div className="title">
          <h1>Dina böcker:</h1>
        </div>
        <div className="books-container"></div>
        <div className="box">
          <p>
            <span className="left">"</span> du är aldrig ensam
          </p>
          <p>
            när du läser <span className="right"> " </span>
          </p>
        </div>
      </section>
    </div>
  );
}

export default Main;
