import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import PlayCircleIcon from "@mui/icons-material/PlayCircle";
import FavoriteIcon from "@mui/icons-material/Favorite";

import AudioPlayerDialog from "../../playerView/AudioPlayerDialog";






function Book() {

 
  return (
    <>
      <Card className="book">
        <CardContent className="book-card-img">
          <Typography sx={{ fontSize: 20 }}>bok titel</Typography>
          <Typography sx={{ fontSize: 15 }}>författare</Typography>
        </CardContent>
        <CardActions className="actions">
          <Button>
            <PlayCircleIcon className="color" />
          </Button>
          <Button size="small">
            <FavoriteIcon
            />
          </Button>
        </CardActions>
      </Card>
      <AudioPlayerDialog/>
    </>
  );
}

export default Book;
