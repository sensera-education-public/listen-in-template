
const books = [
  {
    id: 1,
    title: "Gösta Berling",
    author: "Selma Lagerlöf",
    liked: false,
  },
  {
    id: 2,
    title: "Winter",
    author: "Ali Smith",
    liked: false,
  },
  {
    id: 3,
    title: "The Catcher in the Rye",
    author: "J.D. Salinger",
    liked: false,
  },
  {
    id: 4,
    title: "Asymetry",
    author: "Lisa Feild",
    liked: false,
  },
  {
    id: 5,
    title: "Wonderland",
    author: "Karl Henriksson",
    liked: false,
  },
  {
    id: 6,
    title: "The Hours",
    author: "Michael Cunningham",
    liked: false,
  },
  {
    id: 7,
    title: "View",
    author: "Jane Snoword",
    liked: false,
  },
  {
    id: 8,
    title: "Summer",
    author: "Ali Smith",
    liked: false,
  },
  {
    id: 9,
    title: "Ett Dockhem",
    author: "Henrik Ibsen",
    liked: false,
  },
  
]

function Library() {
 

  return (
    <div className="bg">
      <section className="border">
        <div className="title">
          <h1>Hitta nått att läsa:</h1>
        </div>
        <div className="center">
          <div className="lib-books-container"></div>
        </div>
      </section>
    </div>
  );
}

export default Library;
