import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import FavoriteIcon from "@mui/icons-material/Favorite";



function LibraryBook() {

  return (
    <>
      <Card sx={{ minWidth: 120, }} className="li-book">
        <CardContent className="library-book">
          <Typography sx={{ fontSize: 15 }}>bok titel</Typography>
        </CardContent>
        <CardActions className="actions" style={{padding:2}}>
        <CardContent className="library-author">
           <Typography sx={{ fontSize: 15 }}>författare</Typography>
        </CardContent>
        <div className="liked-icon">
          <Button  size="small" >
            <FavoriteIcon
            />
          </Button>
          </div>
        </CardActions>
      </Card>
    </>
  );
}

export default LibraryBook;
