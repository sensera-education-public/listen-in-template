

function Header() {
  return (
    <header>
      <div className="hero-header">
        <nav>
          <p>Bibliotek</p> |
          <p>Mina böcker</p>
        </nav>
      </div>
    </header>
  );
}

export default Header;
